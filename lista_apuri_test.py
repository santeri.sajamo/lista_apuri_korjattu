import unittest
import lista_apuri

class Test_Testaalista(unittest.TestCase):
    def testaa_parilliset(self):
        onko = lista_apuri.parillisten_maara([-2, 4, 5, -7, 14])
        self.assertEqual(onko,3)

    def testaa_parittomat(self):
        onko = lista_apuri.parittomien_maara([-2, 4, 5, -7, 14])
        self.assertEqual(onko,2)
    
    def testaa_negatiiviset(self):
        onko = lista_apuri.negatiivisten_maara([-2, 4, 5, -7, 14])
        self.assertEqual(onko, 2)
    